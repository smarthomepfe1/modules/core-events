package com.asm.smarthome.coreevents;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;

@SpringBootApplication
public class CoreEventsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoreEventsApplication.class, args);
    }

    @Bean
    MqttPahoClientFactory clientFactory (){
        var factory = new DefaultMqttPahoClientFactory() ;
        var options = new MqttConnectOptions();
        options.setServerURIs();
    }
}
